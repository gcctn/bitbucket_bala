// # Synchronizing *Transaction State*
// This tutorial assumes that you have already set up the application
// as explained in the Setup tutorial.
module.exports = function TransactionsUsage(app){
  // ## Requesting Transactions

  window.grantConsent = function grantConsent(consentId)
  {

    app.intentManager.grantConsent(transaction.id, consentId);
  }

  // We will *sort* on the field name "id" in ascending order.
  var sort = {
    field: "startdate",
    asc: true
  };

  // Let's start a new *page* request for *transactions*.
  var page = app.intentManager.getTransactions({
    pageSize: 25,
    sort
  });

  // This will handle a 'next' *page* event by getting the second page if there
  // is one.
  page.once('next', function handlePage(nextPage){
    // Note: *nextPage and page.latestPage will be the same things in this
    // handler*
    console.log(nextPage, page.latestPage);
    // Herein we ask for a new **page** if there is data remaing.
    if(page.latestPage.remaining <= 0){
      app.intentManager.getTransactions({page: page.latestPage});
    }
  });

  // Here we make our `subscription` to the *Transaction Store*.
  var subscription = app.store.Transaction.observable
    .subscribe(
      function handleUpdate(transactions){

        // ## Listing Transactions

        transactions.forEach(function readTransaction(transaction){
          // webkit.messageHandlers.console.postMessage(JSON.stringify(transaction, null, 4));
          console.log(transaction);
        });

        // ### Getting Detailed Transactions from Overviews
        // The *meta* property of a **transaction** will be set to
        // **'OVERVIEW'** if the **transaction** was last updated by a **page**
        // request. Here we get each transaction in **'DETAILED'** form.
        transactions
          .filter(function filterForOverviewTransaction(transaction){
            return transaction.meta === 'OVERVIEW';
          })
          .forEach(function getDetailedTransaction(transaction){
            app.intentManager.getTransaction(transaction.id);
          });

        // ### Working with Detailed Transactions
        // Let's print the **'DETAILED'** **transactions** we asked for.
        transactions
          .filter(function filterForDetailedTransaction(transaction){
            // If the `meta` property of `DETAILED` is set we have a `DETAILED`
            // `transaction`.
            return transaction.meta === 'DETAILED';
          })
          .forEach(function logDetailedTransaction(transaction){
            console.log(transaction);
          });

        // #### Finding Consents on Individual Transactions
        // Let's look into the individual transactions for *consent*
        // objects and then grant all `REQUESTED` consents.
        transactions
          .filter(function filterForConsents(transaction){
            // Let's check each `DETAILED` `transaction` for the existence of
            // `transaction.consents`
            return transaction.meta === 'DETAILED' && transaction.consents;
          })
          .filter(function grantRequestedConsents(transaction){
            // Let's filter for `REQUESTED` consents.
            var consents = transaction.consents;
            consents.filter(function filterForConsentRequest(consent){
              // *Tip: You can adapt this logic to filter for `GRANTED` or
              // `DENIED` consents.*
              /*
              The `state` of granted consents is `GRANTED`
              The `state` of denied consents is `DENIED`
               */
              return consent.state === "REQUESTED";
            })
            .forEach(function grantConsent(consent){
              // Let's grant every `REQUESTED` consent. These consents will be
              // updated have the state 'GRANTED'.
              console.log("Grant Consent");
              app.intentManager.grantConsent(transaction.id, consent.id);
              // *Tip: Use
              // `app.intentManager.denyConsent(transaction.id, consent.id)`
              // to deny consents.*
              /*
                To instead deny a `REQUESTED` consent use:
                `app.intentManager.denyConsent(transaction.id, consent.id)`
                the `state` of denied consents will be updated to `DENIED`.
               */

               // *Tip: Use
               // `app.intentManager.revokeConsent(transaction.id, consent.id)`
               // to revoke consents.*
               /*
                 To instead revoke a `GRANTED` consent use:
                 `app.intentManager.revokeConsent(transaction.id, consent.id)`
                 the `state` of revoked consents will be updated to `REVOKED`.
                */
            });
          });

        // #### Finding Consents on Individual Transactions

        // `app.intentManager.revokeConsent(transaction.id, consent.id)`
        // the `state` of revoked consents will be updated to `REVOKED`
      }
    );

  // ## Cleaning Up

  // You need to **dipsose** of your **subscription** after you are done with
  // it. Here we just get rid of it after 8 seconds.
  // setTimeout(function handleTimeout(){
  //   subscription.dispose();
  // }, 8000);
};
