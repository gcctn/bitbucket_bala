// # Synchronizing *Transaction State*
// This tutorial assumes that you have already set up the application
// as explained in the Setup tutorial.
module.exports = function TransactionsUsage(app){
  // ## Requesting Transactions

  window.grantConsent = function grantConsent(transacconsentId, consentId){
    app.intentManager.grantConsent(transacconsentId, consentId);
  }
  window.revokeConsent = function revokeConsent(transacconsentId, consentId){
    app.intentManager.revokeConsent(transacconsentId, consentId);
  }
  window.denyConsent = function denyConsent(transacconsentId, consentId){
    app.intentManager.denyConsent(transacconsentId, consentId);
  }
  window.getTransactions = function getTransactions(){
    var sort = {
      field: "startDate",
      asc: false
    };

    // Let's start a new *page* request for *transactions*.
    var page = app.intentManager.getTransactions({
      pageSize: 100,
      sort: sort
    });
    page.on('next', function handlePage(nextPage){
      // Note: *nextPage and page.latestPage will be the same things in this
      // handler*
      // Herein we ask for a new **page** if there is data remaing.
      if(page.latestPage.remaining <= 0){
        app.intentManager.getTransactions({page: page.latestPage});
      }
    });
  }
  window.getTransaction = function getTransaction(id){
    app.intentManager.getTransaction(id);
  }

  // We will *sort* on the field name "id" in ascending order.


  // This will handle a 'next' *page* event by getting the second page if there
  // is one.


  // Here we make our `subscription` to the *Transaction Store*.
  var subscription = app.store.Transaction.observable
    .subscribe(
      function handleUpdate(transactions){
        webkit.messageHandlers.console.postMessage(JSON.stringify(transactions, null, 4));
      }
    );

  // ## Cleaning Up

  // You need to **dipsose** of your **subscription** after you are done with
  // it. Here we just get rid of it after 8 seconds.
  // setTimeout(function handleTimeout(){
  //   subscription.dispose();
  // }, 8000);
};
