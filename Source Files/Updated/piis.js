// # Synchronizing *Personally Identifiable Information (PII) State*
// This tutorial assumes that you have already set up the application
// as explained in the Setup tutorial.
module.exports = function TransactionsUsage(app){
  window.getPiis = function getPiis(){

      app.intentManager.getPiis({
      pageSize: 100
    });


    // This will handle a 'next' *page* event by getting the second page if there
    // is one.
    page.on('next', function handlePage(nextPage){
      // Note: *nextPage and page.latestPage will be the same things in this
      // handler*
      // Herein we ask for a new **page** if there is data remaing.
      if(page.latestPage.remaining <= 0){
        app.intentManager.getPiis({page: page.latestPage});
      }
    });
  }

  // This tutorial will cover requesting and recieving pii data.
  var subscription = app.store.Pii.observable
    .subscribe(
      function handleUpdate(piis){

        webkit.messageHandlers.console.postMessage(JSON.stringify(piis, null, 4));

      }
    );

  // This starts a new **page** request for **piis**
  // var page = app.intentManager.getPiis({
  //   pageSize: 5
  // });

  // // This will handle a 'next' *page* event by getting the second page if there
  // // is one.
  // page.once('next', function handlePage(nextPage){
  //   // Note: *nextPage and page.latestPage will be the same things in this
  //   // handler*
  //   console.log(nextPage, page.latestPage);
  //   // Herein we ask for a new **page** if there is data remaing.
  //   if(page.latestPage.remaining <= 0){
  //     app.intentManager.getPiis({page: page.latestPage});
  //   }
  // });

  // You need to **dipsose** of your **subscription** after you are done with
  // it. Here we just get rid of it after 8 seconds.
  // setTimeout(function handleTimeout(){
  //   subscription.dispose();
  // }, 8000);
};
