// This just clears the sessionStorage
sessionStorage.clear();
// # Getting Started: Setup
// First create a directory named `myapp`.
//
// In this directory run `npm init` to setup your project.
//
// Then install the **Trunomi SDKs** as a dependency by running
// `npm install --save ./path/to/trunomi/sdk`.
//
// browserify main.js -o bundle.js
// http://ccoenraets.github.io/es6-tutorial/ecmascript6-setup-babel.html

// **You need to implement the following two bank services:**
// - `login({config}, {username, password, withKeys})` that returns a promise
//  - it *resolves* with
// `({authToken, enterpriseID, displayName, username, publicKey, privateKey})`,
//  - otherwise it *rejects* with `({error})`
// - `logout({config, authToken})` that returns a promise
//  - it *resolves* if the logout was successful
//  - otherwise it *rejects* with `({error})`

// *An example of this code can be found in `demo/client/common/services/bank/`*
var EnterpriseServices = require('trunomi/demo/client/common/services/bank');

// *Require* the **Trunomi CSDK**.
var trunomi = require('trunomi/csdk');

// For the **Consumer SDK (CSDK)**, you will need a `config` object
// that defines:
// - `BANK_END_POINT`
// - `TPB_END_POINT`
//
// *Optionally, you can also turn on `DISPATCH_LOGGING`.*

var config = {
  // ENTERPRISE_END_POINT: 'https://fis-gcc-be.trunomi.com',
  // TPB_END_POINT: 'https://fis-gcc-tpb.trunomi.com',
  ENTERPRISE_END_POINT: 'https://fis-devjam-test.apigee.net/tn/bm',
  TPB_END_POINT: 'https://fis-devjam-test.apigee.net/tn/tpb',
  DISPATCH_LOGGING: true
};

// The SDK is setup and we may begin to use it.
module.exports = trunomi(config, EnterpriseServices);
// The call to setupd the SDK returns an object with the `store`
// and the `intentManager`.


// <style>
// div#background{
//   width: 50%;
// }
// ul.sections > li > div.annotation {
//   max-width: 50%;
//   min-width: 50%;
// }
// </style>
