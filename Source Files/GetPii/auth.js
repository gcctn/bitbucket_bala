// # Synchronizing and Sending *Authentication State*
// This tutorial assumes that you have already set up the application
// as explained in the Setup tutorial.
//
// *You can use this file as the entry point to a sample application by using:*
//
// `browserify path/to/this/file.js -o app.js`
var app = require('./setup');

// The most basic flow covered is logging in a consumer.
// There are three parts to this flow:
//  1. Recieving current login information throught your application
//  2. Sending a valid login intent.
//  3. Sending the hello intent to the tpb.

// This is a subscription to the current *Authentication state*.
// You can have as many as you want and they will always be in sync.

window.startAuth = function startAuth(username, password){
  webkit.messageHandlers.authConsole.postMessage("Inside startAuth");
  var subscription = app.store.Auth.observable
    .subscribe(
      function handleUpdate(status){
      	webkit.messageHandlers.authConsole.postMessage(JSON.stringify(status, null, 4));
        if(status.loginError === true){
          console.warn("There was an error logging in!");
        }
        else if(status.loginStatus === true && !status.sessionID){
          console.info(`Success, ${status.username} has been logged in.`);
          // This means that we are *logged in* (to an Enterprise), but have not
          // yet recieved a Trunomi **sessionID**. Let's say **hello**:
          app.intentManager.hello();
        }
        else if(status.loginStatus === true && status.sessionID){
          // This means that we are *logged in* and have authenticated our session
          // with Trunomi.
          // We can now start using the other stores and intents...

          // **See [Usage Transactions](Usage_Transactions.md.html).**
          require('./transactions')(app);
          // **See [Usage Pii](Usage_Pii.md.html).**
          require('./piis')(app);
        }
        else {
          // app.intentManager.login("mark_watney", "trunomi123");
          app.intentManager.login(username, password);
          window.API = app.API;

        }
      }
    );

  // The **intentManager** will attempt to login for you. This happens when this code is loaded, but after the subscription handler is setup.

  // You need to **dipsose** of your **subscription** after you are done with it.
  // Here we just get rid of it after 8 seconds.
  setTimeout(function handleTimeout(){
    subscription.dispose();
  }, 8000);
}
