// # Synchronizing *Personally Identifiable Information (PII) State*
// This tutorial assumes that you have already set up the application
// as explained in the Setup tutorial.
module.exports = function TransactionsUsage(app){

  // This tutorial will cover requesting and recieving pii data.
  var subscription = app.store.Pii.observable
    .subscribe(
      function handleUpdate(piis){
        // The *meta* property of a **pii** will be set to **'OVERVIEW'**
        // if the **pii** was last updated by a **page** request.
        // Let's print the **'OVERVIEW'** **piis** we asked for.
        piis
          .forEach(function logPii(pii){
            console.log(pii);
            webkit.messageHandlers.piiConsole.postMessage(JSON.stringify(pii, null, 4));
          });
          webkit.messageHandlers.endPiiConsole.postMessage('End Pii');
      }
    );

  // This starts a new **page** request for **piis**
  var page = app.intentManager.getPiis({
    pageSize: 5
  });

  // This will handle a 'next' *page* event by getting the second page if there
  // is one.
  page.once('next', function handlePage(nextPage){
    // Note: *nextPage and page.latestPage will be the same things in this
    // handler*
    console.log(nextPage, page.latestPage);
    // Herein we ask for a new **page** if there is data remaing.
    if(page.latestPage.remaining <= 0){
      app.intentManager.getPiis({page: page.latestPage});
    }
  });

  // You need to **dipsose** of your **subscription** after you are done with
  // it. Here we just get rid of it after 8 seconds.
  setTimeout(function handleTimeout(){
    subscription.dispose();
  }, 8000);
};
