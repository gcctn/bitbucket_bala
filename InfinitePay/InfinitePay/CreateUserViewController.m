//
//  CreateUserViewController.m
//  InfinitePay
//
//  Created by Balakrishnan on 6/22/16.
//  Copyright © 2016 FIS Global, Inc. All rights reserved.
//
#import "InfinitePay-Swift.h"

#import "CreateUserViewController.h"
#import "InfinitePay-Bridging-Header.h"

@interface CreateUserViewController ()

@end

@implementation CreateUserViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    
    NSLog(@"Inside CreateUserViewController: viewDidLoad");
    
    [_userLabel performSelectorOnMainThread:@selector(setText:) withObject:@"" waitUntilDone:NO];
    //    [_companyLabel performSelectorOnMainThread:@selector(setText:) withObject:@"" waitUntilDone:NO];
    //    [_addressLabel performSelectorOnMainThread:@selector(setText:) withObject:@"" waitUntilDone:NO];
    //    [_emailLabel performSelectorOnMainThread:@selector(setText:) withObject:@"" waitUntilDone:NO];
    
    //    [_registerButton setEnabled:false];
    //    [_registerButton setAlpha:0.5];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)scanUser:(id)sender {
    
    if (!_isReading) {
        // This is the case where the app should read a QR code when the viewPreview is tapped.
        if ([self startReading]) {
            // If the startReading methods returns YES and the capture session is successfully
            // running, then change the start button title and the status message.
            //                        [_bbitemStart setTitle:@"Stop" forState:UIControlStateNormal];
            
            //            [_viewPreview setHidden:NO];
            
            _userId = NULL;
            
            [_userLabel setText:@"Scanning"];
            //            [_companyLabel performSelectorOnMainThread:@selector(setText:) withObject:@"" waitUntilDone:NO];
            //            [_addressLabel performSelectorOnMainThread:@selector(setText:) withObject:@"" waitUntilDone:NO];
            //            [_emailLabel performSelectorOnMainThread:@selector(setText:) withObject:@"" waitUntilDone:NO];
        }
    }
    else{
        // In this case the app is currently reading a QR code and it should stop doing so.
        [self stopReading];
        
        // The bar button item's title should change again.
        //                [_bbitemStart setTitle:@"Start" forState:UIControlStateNormal];
    }
    
    // Set to the flag the exact opposite value of the one that currently has.
    _isReading = !_isReading;
}

- (BOOL)startReading {
    NSError *error;
    
    // Get an instance of the AVCaptureDevice class to initialize a device object and provide the video
    // as the media type parameter.
    AVCaptureDevice *captureDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    
    // Get an instance of the AVCaptureDeviceInput class using the previous device object.
    AVCaptureDeviceInput *input = [AVCaptureDeviceInput deviceInputWithDevice:captureDevice error:&error];
    
    if (!input) {
        // If any error occurs, simply log the description of it and don't continue any more.
        NSLog(@"%@", [error localizedDescription]);
        return NO;
    }
    
    // Initialize the captureSession object.
    _captureSession = [[AVCaptureSession alloc] init];
    // Set the input device on the capture session.
    [_captureSession addInput:input];
    
    // Initialize a AVCaptureMetadataOutput object and set it as the output device to the capture session.
    AVCaptureMetadataOutput *captureMetadataOutput = [[AVCaptureMetadataOutput alloc] init];
    [_captureSession addOutput:captureMetadataOutput];
    
    // Create a new serial dispatch queue.
    dispatch_queue_t dispatchQueue;
    dispatchQueue = dispatch_queue_create("myQueue", NULL);
    [captureMetadataOutput setMetadataObjectsDelegate:self queue:dispatchQueue];
    [captureMetadataOutput setMetadataObjectTypes:[NSArray arrayWithObject:AVMetadataObjectTypeQRCode]];
    
    // Initialize the video preview layer and add it as a sublayer to the viewPreview view's layer.
    _videoPreviewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:_captureSession];
    [_videoPreviewLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    [_videoPreviewLayer setFrame:_viewPreview.layer.bounds];
    [_viewPreview.layer addSublayer:_videoPreviewLayer];
    
    // Start video capture.
    [_captureSession startRunning];
    
    return YES;
}

-(void)stopReading{
    // Stop video capture and make the capture session object nil.
    [_captureSession stopRunning];
    _captureSession = nil;
    
    // Remove the video preview layer from the viewPreview view's layer.
    [_videoPreviewLayer removeFromSuperlayer];
    
    [_userLabel performSelectorOnMainThread:@selector(setText:) withObject:_userId waitUntilDone:NO];
}

-(void)captureOutput:(AVCaptureOutput *)captureOutput didOutputMetadataObjects:(NSArray *)metadataObjects fromConnection:(AVCaptureConnection *)connection{
    
    // Check if the metadataObjects array is not nil and it contains at least one object.
    if (metadataObjects != nil && [metadataObjects count] > 0) {
        // Get the metadata object.
        AVMetadataMachineReadableCodeObject *metadataObj = [metadataObjects objectAtIndex:0];
        if ([[metadataObj type] isEqualToString:AVMetadataObjectTypeQRCode]) {
            // If the found metadata is equal to the QR code metadata then update the name, email fields
            // stop reading and change the bar button item's title and the flag's value.
            // Everything is done on the main thread.
            //            [self.userName performSelectorOnMainThread:@selector(setText:) withObject:[metadataObj stringValue] waitUntilDone:NO];
            
            //            NSString *string = @"oop:ack:bork:greeble:ponies";
            NSString *qrCode = [metadataObj stringValue];
            NSLog(@"QR Code: %@", qrCode);
            
            //            [self writeToTextFile:qrCode];
            
            //            NSArray *chunks = [qrCode componentsSeparatedByString: @"|"];
            //            NSLog(@"%@ - %@ - %@ - %@", chunks [0], chunks [1], chunks [2], chunks[3]);
            
            NSArray *chunks = [qrCode componentsSeparatedByString: @"^"];
            NSLog(@"%@ - %@ - %@ - %@", chunks [0], chunks [1], chunks [2], chunks[3]);
            
            
            //            qrCode = chunks [0];
            
            
            //            [self.userName performSelectorOnMainThread:@selector(setText:) withObject:chunks[0] waitUntilDone:NO];
            //            [self.email performSelectorOnMainThread:@selector(setText:) withObject:chunks[3] waitUntilDone:NO];
            //            [_userLabel performSelectorOnMainThread:@selector(setText:) withObject:qrCode waitUntilDone:NO];
            //            if(_userLabel.text==@"Scanning")
            //            {
            //                //                [GlobalObjects setUser:qrCode];
            //                //                [self saveQRReadName:qrCode table:@"ScannedUsers"];
            //            }
            //            else
            //            {
            //                //                [GlobalObjects setPresenter:qrCode];
            //                //                [self saveQRReadName:qrCode table:@"ScannedPresenters"];
            //            }
            if(_userLabel.text==@"Scanning")
            {
                _userId = @"Scan Complete";
                _emailId = chunks[3];
                _qrCode = qrCode;
                [self performSelectorOnMainThread:@selector(stopReading) withObject:nil waitUntilDone:NO];
                
                [self createUser];
                
                //                NSString* name = [NSString stringWithFormat:@"%@%@%@%@",@"Name: ", chunks[0], @" ", chunks[1]];
                //                NSString* company = [NSString stringWithFormat:@"%@%@",@"Company: ", chunks[2]];
                //                NSString* address = [NSString stringWithFormat:@"%@%@%@%@",@"Address: ", chunks[3], @", ", chunks[4]];
                //                NSString* email = [NSString stringWithFormat:@"%@%@",@"Email: ", chunks[5]];
                
                //                [_userLabel performSelectorOnMainThread:@selector(setText:) withObject:@"Scan Complete" waitUntilDone:NO];
                //                [_companyLabel performSelectorOnMainThread:@selector(setText:) withObject:company waitUntilDone:NO];
                //                [_addressLabel performSelectorOnMainThread:@selector(setText:) withObject:address waitUntilDone:NO];
                //                [_emailLabel performSelectorOnMainThread:@selector(setText:) withObject:email waitUntilDone:NO];
                
                //                _userId = name;
                
                //                [_viewPreview setHidden:YES];
                
                //                [_registerButton setEnabled:true];
            }
            
            //            [self setLabel: qrCode];
            
            //                        [_bbitemStart performSelectorOnMainThread:@selector(setTitle:) withObject:@"StartTap" waitUntilDone:NO];
            
            
            
            _isReading = NO;
            
            // Capture the Bank name from the information captured
            // e.g. Liz|Gaston|ARCA|lizg@arcatechsystems.com, so capture the string betweek @ and .
            
            //            NSString *bankName = [self extractString: [metadataObj stringValue] toLookFor:@"@" toStopBefore:@"."];
            
            
            //self.bbitemSentimentPOC.hidden = NO;
            
            // If the audio player is not nil, then play the sound effect.
            //            if (_audioPlayer) {
            //                [_audioPlayer play];
            //            }
        }
    }
}

- (IBAction)doRegister:(id)sender {
    
    [self createUser];
    
}

- (void) createUser{
    
    NSLog(@"Inside createUser");
    
    NSLog(_qrCode);
    
    NSArray *chunks = [_qrCode componentsSeparatedByString: @"^"];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]
                                    initWithURL:[NSURL
                                                 URLWithString:@"https://fis-devjam-test.apigee.net/tn/bm/v1/bankmock/cfa/app/create_user"]];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    //    NSDictionary *params = @{
    //                                @"password" : @"15514a27d89d0b69",
    //                                @"user": @{
    //                                            @"email":chunks[5],
    ////                                            @"first_name":chunks[0],
    ////                                            @"last_name":chunks[1],
    //                                            @"gender": @"Not Available",
    //                                            @"address":[NSString stringWithFormat:@"%@%@%@", chunks[3], @", ", chunks[4]],
    ////                                            @"date_of_birth": @"NA",
    //                                            @"nationality": @"Not Available",
    //                                            @"phone":@"Not Available",
    //                                            @"password":@"trunomi123",
    //                                            @"username":chunks[5],
    //                                            @"first":chunks[0],
    //                                            @"last":chunks[1],
    //                                            @"dob": @"Not Available"
    //                                           }
    //                                };
    
    NSDictionary *params = @{
                             @"password" : @"15514a27d89d0b69",
                             @"user": @{
                                     @"email":chunks[3],
                                     //                                            @"first_name":chunks[0],
                                     //                                            @"last_name":chunks[1],
                                     @"gender": @"Not Available",
                                     @"address":[NSString stringWithFormat:@"%@%@%@", chunks[6], @", ", chunks[7]],
                                     //                                            @"date_of_birth": @"NA",
                                     @"nationality": @"Not Available",
                                     @"phone":@"Not Available",
                                     @"password":@"trunomi123",
                                     @"username":chunks[3],
                                     @"first":chunks[2],
                                     @"last":chunks[1],
                                     @"dob": @"Not Available"
                                     }
                             };
    
    NSError* error;
    NSData* postString = [NSJSONSerialization dataWithJSONObject:params options:NSJSONWritingPrettyPrinted error: &error];
    [request setHTTPBody:postString];
    
    
    NSLog(@"postString");
    //    NSLog(postString);
    
    [request setValue:[NSString stringWithFormat:@"%lu",(unsigned long)[postString length]] forHTTPHeaderField:@"Content-length"];
    
    __block NSMutableDictionary *resultsDictionary;
    __block NSError *error1;
    
    NSLog(@"postString1");
    
    NSURLSessionDataTask *dataTask = [[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        // handle request error
        if (error) {
            //            completion(nil, error);
            NSLog(@"postString2");
            return;
        }
        NSLog(@"Response:%@ %@\n", response, error);
        if(error == nil)
        {
            NSString * text = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
            NSLog(@"Data = %@",text);
        }
        
        
        NSError *myError;
        NSLog(@"postString3");
        NSMutableDictionary *returnDdata = [[NSMutableDictionary alloc]initWithDictionary:[NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&myError]];
        //        NSLog(productionSchedule);
        //        completion(productionSchedule,myError);
        if(myError==nil)
        {
            if([returnDdata objectForKey:@"error"])
            {
                if([returnDdata objectForKey:@"username"])
                {
                    //                    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    //                        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Duplicate"
                    //                                                                            message:@"User already exists. Let's proceed to next screen."
                    //                                                                           delegate:self
                    //                                                                  cancelButtonTitle:@"OK"
                    //                                                                  otherButtonTitles:nil, nil];
                    //                        [alertView show];
                    UserObject.username = [returnDdata objectForKey:@"username"];
                    //                    }];
                }
                else
                {
                    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                                            message:@"There was a problem in the Registration."
                                                                           delegate:self
                                                                  cancelButtonTitle:@"OK"
                                                                  otherButtonTitles:nil, nil];
                        [alertView show];
                        return ;
                    }];
                    
                }
            }
            else
            {
                //                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                //                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Registration Successful"
                //                                                                        message:@"Scanned user added successfully."
                //                                                                       delegate:self
                //                                                              cancelButtonTitle:@"OK"
                //                                                              otherButtonTitles:nil, nil];
                //                    [alertView show];
                UserObject.username = [returnDdata objectForKey:@"username"];
                //                }];
            }
        }
        else
        {
            NSLog(@"Error in parsing");
        }
        
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            
            
            ConsentAcceptViewController *consentAcceptViewController =
            [self.storyboard instantiateViewControllerWithIdentifier:@"ConsentAcceptViewController"];
            
            [self presentViewController:consentAcceptViewController
                               animated:YES
                             completion:nil];
            
        }];
        
        
    }];
    
    [dataTask resume];
    
    
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
