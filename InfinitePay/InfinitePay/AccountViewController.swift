//
//  AccountViewController.swift
//  InfinitePay
//
//  Created by Balakrishnan on 6/10/16.
//  Copyright © 2016 FIS Global, Inc. All rights reserved.
//

import UIKit

class AccountViewController: UIViewController {

    @IBOutlet weak var dueDateLabel: UILabel!
    @IBOutlet weak var balanceLabel: UILabel!
    
    @IBOutlet weak var makePaymentButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if(UserObject.balance==nil)
        {
            self.getBalance()
        }
        else
        {
            self.balanceLabel.text = "Available Balance: $" + String(UserObject.balance!)
        }
        getTomorrowDate()
//        self.consumerIDLabel.text =  "Customer ID: " + UserObject.consumerID!

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated) // No need for semicolon
        print("Inside viewWillAppear")
        if(!(UserObject.balance==nil))
        {
            self.balanceLabel.text = "Available Balance: $" + String(UserObject.balance!)
        }
        
    }
    
    func getTomorrowDate()
    {
        print("Inside getTomorrowDate")
        let dateFormater = NSDateFormatter()
        dateFormater.dateFormat = "MM/dd/yy"
        let tomorrowDate = dateFormater.stringFromDate(NSDate().dateByAddingTimeInterval(24 * 60 * 60))
        self.dueDateLabel.text = "Due date: " + tomorrowDate
        print(tomorrowDate)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getBalance(data:NSData) -> NSInteger
    {
        var balance:NSInteger = 12
        var err: NSError?
        do{
            
            var json = try NSJSONSerialization.JSONObjectWithData(data, options: .MutableLeaves) as? NSDictionary
            
            // Did the JSONObjectWithData constructor return an error? If so, log the error to the console
            if(err != nil) {
                print(err!.localizedDescription)
                let jsonStr = NSString(data: data, encoding: NSUTF8StringEncoding)
                print("Error could not parse JSON: '\(jsonStr)'")
            }
            else {
                // The JSONObjectWithData constructor didn't return an error. But, we should still
                // check and make sure that json has a value using optional binding.
                if let parseJSON = json {
                    // Okay, the parsedJSON is here, let's get the value for 'success' out of it
                    balance = parseJSON["balance"] as! NSInteger
                    //                        print("Succes: \(success)")
                }
            }
        }catch {
            print(error)
        }
        
        return balance
        
    }
    
    
    @IBAction func makePayment(sender: AnyObject) {
        
        setBalance(UserObject.balance! - 5)
//        self.tabBarController!.selectedIndex = 1
        
    }
    
    func getBalance()
    {
        var authToken = UserObject.authToken!
        //let request = NSMutableURLRequest(URL: NSURL(string: "https://fis-gcc-bankmock.trunomi.com/v1/bankmock/cfa/app/account_balance")!)
        let request = NSMutableURLRequest(URL: NSURL(string: "https://fis-devjam-test.apigee.net/tn/bm/v1/bankmock/cfa/app/account_balance")!)
        
        request.HTTPMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        var params = ["authToken":authToken] as Dictionary<String, String>
        do
        {
            let postString = try NSJSONSerialization.dataWithJSONObject(params, options: .PrettyPrinted)
            request.HTTPBody = postString
            request.setValue(String(request.HTTPBody?.length), forHTTPHeaderField: "Content-Length")
            print(request.HTTPBody?.length)
            let task = NSURLSession.sharedSession().dataTaskWithRequest(request) { data, response, error in
                guard error == nil && data != nil else {                                                          // check for fundamental networking error
                    print("error=\(error)")
                    return
                }
                
                if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200 {           // check for http errors
                    print("statusCode should be 200, but is \(httpStatus.statusCode)")
                    print("response = \(response)")
                    let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
                    print("responseString = \(responseString)")
                }
                
                let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
                print("responseString = \(responseString)")
                var balance = self.getBalance(data!)
                
                //                self.nameLabel.performSelector("text", withObject: name)
                //                self.nameLabel.text
                NSOperationQueue.mainQueue().addOperationWithBlock {
                    self.balanceLabel.text =  "Available Balance: $" + String(balance)
                    UserObject.balance = balance
//                    UserObject.consumerID = consumerID
                }
                //                UserObject.authToken = authToken
            }
            task.resume()
            
        } catch {
            print(error)
        }
    }
    
    func setBalance(newBalance:NSInteger)
    {
        var authToken = UserObject.authToken!
        //let request = NSMutableURLRequest(URL: NSURL(string: "https://fis-gcc-bankmock.trunomi.com/v1/bankmock/cfa/app/account_balance")!)
        let request = NSMutableURLRequest(URL: NSURL(string: "https://fis-devjam-test.apigee.net/tn/bm/v1/bankmock/cfa/app/account_balance")!)
        request.HTTPMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        var params = ["authToken":authToken, "set": newBalance] as Dictionary<String, NSObject>
        do
        {
            let postString = try NSJSONSerialization.dataWithJSONObject(params, options: .PrettyPrinted)
            request.HTTPBody = postString
            request.setValue(String(request.HTTPBody?.length), forHTTPHeaderField: "Content-Length")
            print(request.HTTPBody?.length)
            let task = NSURLSession.sharedSession().dataTaskWithRequest(request) { data, response, error in
                guard error == nil && data != nil else {                                                          // check for fundamental networking error
                    print("error=\(error)")
                    return
                }
                
                if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200 {           // check for http errors
                    print("statusCode should be 200, but is \(httpStatus.statusCode)")
                    print("response = \(response)")
                    let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
                    print("responseString = \(responseString)")
                }
                
                let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
                print("responseString = \(responseString)")
                var balance = self.getBalance(data!)
                
                //                self.nameLabel.performSelector("text", withObject: name)
                //                self.nameLabel.text
                NSOperationQueue.mainQueue().addOperationWithBlock {
                    self.balanceLabel.text = "Available Balance: $" + String(balance)
                    UserObject.balance = balance
                    let alert = UIAlertController(title: "Payment Successful", message: "Payment of $5 was successfully made.", preferredStyle: UIAlertControllerStyle.Alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { action in
                        switch action.style{
                        case .Default:
                            print("default")
                            self.makePaymentButton.enabled = false
                            self.makePaymentButton.alpha = 0.5
                            
                        case .Cancel:
                            print("cancel")
                            
                        case .Destructive:
                            print("destructive")
                        }
                    }))
                    self.presentViewController(alert, animated: true, completion: nil)
                }
                //                UserObject.authToken = authToken
            }
            task.resume()
            
        } catch {
            print(error)
        }
    }
    


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
