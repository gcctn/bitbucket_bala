//
//  PermissionsViewController.swift
//  InfinitePay
//
//  Created by Balakrishnan on 6/10/16.
//  Copyright © 2016 FIS Global, Inc. All rights reserved.
//

import UIKit
import WebKit

class PermissionsViewController: UIViewController, WKScriptMessageHandler {
    
    @IBOutlet weak var removeSwitch: UISwitch!
    
    @IBOutlet weak var webView: UIWebView!
    var webKitView: WKWebView?;
    var scriptController: WKUserContentController?;
    
    func getScriptController() -> WKUserContentController {
        if (self.scriptController == nil){
            self.scriptController = WKUserContentController();
        }
        return self.scriptController!;
    }
    
    func addScriptToWK(script: String) {
        let scriptController = getScriptController();
        let startAuth = WKUserScript(
            source: script,
            injectionTime: WKUserScriptInjectionTime.AtDocumentStart,
            forMainFrameOnly: true
        );
        scriptController.addUserScript(startAuth);
    }
    
    func clearWKCache() -> Void {
        if #available(iOS 9.0, *) {
            let websiteDataTypes = NSSet(array: [WKWebsiteDataTypeDiskCache, WKWebsiteDataTypeMemoryCache])
            let date = NSDate(timeIntervalSince1970: 0)
            WKWebsiteDataStore.defaultDataStore().removeDataOfTypes(websiteDataTypes as! Set<String>, modifiedSince: date, completionHandler:{ })
        }
    }
    
    override func loadView(){
        super.loadView();
        clearWKCache();
        //        prepareResources();
        let config = WKWebViewConfiguration();
        config.userContentController = getScriptController();
        config.userContentController.addScriptMessageHandler(self, name: "console");
        config.userContentController.addScriptMessageHandler(self, name: "authConsole");
        config.userContentController.addScriptMessageHandler(self, name: "revokeConsentConsole");
        
        self.webKitView = WKWebView(frame: self.webView.bounds, configuration: config);
        self.webView.addSubview(webKitView!);
    }
    
    func userContentController(userContentController: WKUserContentController, didReceiveScriptMessage message: WKScriptMessage) {
        
        print(message.name)
        if(message.name=="console")
        {
            print(message.body)
        }
        else if(message.name=="authConsole")
        {
            print(message.body)
        }
        else if(message.name=="revokeConsentConsole")
        {
            print(message.body)
            self.revokeConsentHandle()
            
        }
        
    }
    
    func revokeConsentHandle()
    {
        NSOperationQueue.mainQueue().addOperationWithBlock
            {
                UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                let alert = UIAlertController(title: "Revoke Successful", message: "The Consent was revoked successfully. you will be taken to the InfinitePay home screen.", preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { action in
                    switch action.style{
                    case .Default:
                        print("default")
                        let tabVC =  self.storyboard!.instantiateViewControllerWithIdentifier("MainViewController") as! MainViewController
                        self.presentViewController(tabVC, animated: true, completion: nil)
                    case .Cancel:
                        print("cancel")
                        
                    case .Destructive:
                        print("destructive")
                    }
                }))
                self.presentViewController(alert, animated: true, completion: nil)
        }

    }
    
    
    override func viewDidLoad() {

        super.viewDidLoad()
        print("End viewDidLoad")
        // Do any additional setup after loading the view.
        
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func removeAll(sender: AnyObject) {
        
        if(removeSwitch.on)
        {
            print("ON")
        }
        else
        {
            print("OFF")
            
            let alert = UIAlertController(title: "Revoke Consent", message: "This action will revoke the permissions shared with us. Do you wish to continue?", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "Proceed", style: .Destructive, handler: { action in
                switch action.style{
                case .Default:
                    print("default")
                    
                case .Cancel:
                    print("cancel")
                    
                case .Destructive:
                    print("Proceeding to revoke consent")
                    self.revokeConsent()
                    
                }
            }))
            alert.addAction(UIAlertAction(title: "Decline", style: .Cancel, handler: { action in
                switch action.style{
                case .Default:
                    print("default")
                    
                case .Cancel:
                    print("Declining to stay on the page")
                    self.removeSwitch.setOn(true, animated: true)
                    
                case .Destructive:
                    print("destructive")
                }
            }))
            self.presentViewController(alert, animated: true, completion: nil)
            
            
            
        }
    }
    
    func revokeConsent()
    {
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        let url = NSURL.fileURLWithPath(NSBundle.mainBundle().pathForResource("index", ofType: "html")!)
        let req = NSURLRequest(URL: url);
        self.webKitView!.loadRequest(req);
        let pathToJS = NSBundle.mainBundle().pathForResource("revokeConsent", ofType: "js")!
        let js = try! String(contentsOfFile: pathToJS, encoding: NSUTF8StringEncoding);
        self.addScriptToWK(js);
        let userName:String = UserObject.username!
        var password:String
        if let val = DataRepository.credentials[userName]
        {
            password = val
        }
        else
        {
            password = "trunomi123"
        }
        
        print(userName)
        print(password)
        
        self.addScriptToWK("startAuth('" + userName + "', '" + password + "')");
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
