//
//  LoginViewController.swift
//  InfinitePay
//
//  Created by Balakrishnan on 6/23/16.
//  Copyright © 2016 FIS Global, Inc. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    @IBOutlet weak var customerNameTextField: UITextField!
    
    @IBOutlet weak var scanButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        let myColor : UIColor = UIColor( red: 1, green: 1, blue:1, alpha: 1.0 )
        scanButton.layer.borderColor = myColor.CGColor
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        view.addGestureRecognizer(tap)
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    @IBAction func dismissKeyBoardOnEnter(sender: AnyObject) {
        customerNameTextField.resignFirstResponder()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func doLogin(sender: AnyObject) {
        
        print("Inside doLogin")
        customerNameTextField.resignFirstResponder()
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        let userName:String = customerNameTextField.text!
        if(userName.isEmpty)
        {
            let alert = UIAlertController(title: "Empty", message: "Username field can't be empty.", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
            return ;
        }
        
        var password:String
        if let val = DataRepository.credentials[userName] {
            password = val
        }
        else{
            password = "trunomi123"
        }
        print(userName)
        print(password)
        
        let request = NSMutableURLRequest(URL: NSURL(string: DataRepository.loginURL)!)
        
        request.HTTPMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        var params = ["username":userName, "password":password] as Dictionary<String, String>
        
        do {
            let postString = try NSJSONSerialization.dataWithJSONObject(params, options: .PrettyPrinted)
            request.HTTPBody = postString
            request.setValue(String(request.HTTPBody?.length), forHTTPHeaderField: "Content-Length")
            print(request.HTTPBody?.length)
            let task = NSURLSession.sharedSession().dataTaskWithRequest(request) { data, response, error in
                guard error == nil && data != nil else {                                                          // check for fundamental networking error
                    print("error=\(error)")
                    UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                    NSOperationQueue.mainQueue().addOperationWithBlock {
                        let alert = UIAlertController(title: "Network Error", message: "Not able to connect to Internet. Check your Wi-Fi and Data settings.", preferredStyle: UIAlertControllerStyle.Alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: nil))
                        self.presentViewController(alert, animated: true, completion: nil)
                        UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                        return
                    }
                    return
                }
                
                if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200 {           // check for http errors
                    print("statusCode should be 200, but is \(httpStatus.statusCode)")
                    print("response = \(response)")
                    let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
                    print("responseString = \(responseString)")
                    NSOperationQueue.mainQueue().addOperationWithBlock {
                        let alert = UIAlertController(title: "Invalid", message: "Username doesn't exist. Please create one by scanning the badge.", preferredStyle: UIAlertControllerStyle.Alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: nil))
                        self.presentViewController(alert, animated: true, completion: nil)
                        UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                        return
                    }
                }
                else
                {
                    
                    let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
                    print("responseString = \(responseString)")
                    self.getAuthDetails(data!)
                    UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                    if(UserObject.displayName!.isEmpty)
                    {
                        return
                    }
                    UserObject.username = userName
                    NSOperationQueue.mainQueue().addOperationWithBlock {
//                        let consentAcceptViewController =  self.storyboard!.instantiateViewControllerWithIdentifier("ConsentAcceptViewController") as! ConsentAcceptViewController
//                        self.presentViewController(consentAcceptViewController, animated: true, completion: nil)
                        self.getKeys()
                    }
                }
            }
            task.resume()
            
        } catch {
            print(error)
        }
        
    }
    
    func getAuthDetails(data:NSData)
    {
        var err: NSError?
        do{
            
            var json = try NSJSONSerialization.JSONObjectWithData(data, options: .MutableLeaves) as? NSDictionary
            
            // Did the JSONObjectWithData constructor return an error? If so, log the error to the console
            if(err != nil) {
                print(err!.localizedDescription)
                let jsonStr = NSString(data: data, encoding: NSUTF8StringEncoding)
                print("Error could not parse JSON: '\(jsonStr)'")
                let alert = UIAlertController(title: "JSON Error", message: "Response is not in JSON format. Try again.", preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: nil))
                self.presentViewController(alert, animated: true, completion: nil)
                return
            }
            else {
                if let parseJSON = json {
                    if let auth = parseJSON["authToken"]{
                        var authToken = auth as! String
                        UserObject.authToken = authToken
                    }
                    else
                    {
                        let alert = UIAlertController(title: "Response Error", message: "authToken is not returned. Try again.", preferredStyle: UIAlertControllerStyle.Alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: nil))
                        self.presentViewController(alert, animated: true, completion: nil)
                        return
                    }
                    if let entId = parseJSON["enterpriseID"]{
                         var enterpriseId = entId as! String
                        UserObject.enterpriseID = enterpriseId
                    }
                    else
                    {
                        let alert = UIAlertController(title: "Response Error", message: "enterpriseID is not returned. Try again.", preferredStyle: UIAlertControllerStyle.Alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: nil))
                        self.presentViewController(alert, animated: true, completion: nil)
                        return
                    }
                    if let firstName = parseJSON["fname"]{
                        var name = firstName as! String
                        UserObject.displayName = name
                    }
                    else
                    {
                        let alert = UIAlertController(title: "Response Error", message: "fname is not returned. Try again.", preferredStyle: UIAlertControllerStyle.Alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: nil))
                        self.presentViewController(alert, animated: true, completion: nil)
                        return
                    }
                    if let lastName = parseJSON["lname"]{
                        var name = lastName as! String
                        UserObject.displayName = UserObject.displayName! + " " + name
                    }
                    else
                    {
                        let alert = UIAlertController(title: "Response Error", message: "lname is not returned. Try again.", preferredStyle: UIAlertControllerStyle.Alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: nil))
                        self.presentViewController(alert, animated: true, completion: nil)
                        return
                    }
                }
                else
                {
                    let alert = UIAlertController(title: "Response Error", message: "Response is empty. Try again.", preferredStyle: UIAlertControllerStyle.Alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: nil))
                    self.presentViewController(alert, animated: true, completion: nil)
                    return
                }
            }
        }catch{
            print(error)
        }
    }
    
    func getKeys()
    {
        let request = NSMutableURLRequest(URL: NSURL(string: DataRepository.consumerKeysURL)!)
        
        request.HTTPMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        var params = ["authToken":UserObject.authToken!] as Dictionary<String, String>
        
        do {
            let postString = try NSJSONSerialization.dataWithJSONObject(params, options: .PrettyPrinted)
            request.HTTPBody = postString
            request.setValue(String(request.HTTPBody?.length), forHTTPHeaderField: "Content-Length")
            print(request.HTTPBody?.length)
            let task = NSURLSession.sharedSession().dataTaskWithRequest(request) { data, response, error in
                guard error == nil && data != nil else {                                                          // check for fundamental networking error
                    print("error=\(error)")
                    UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                    NSOperationQueue.mainQueue().addOperationWithBlock {
                        let alert = UIAlertController(title: "Network Error", message: "Not able to connect to Internet. Check your Wi-Fi and Data settings.", preferredStyle: UIAlertControllerStyle.Alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: nil))
                        self.presentViewController(alert, animated: true, completion: nil)
                        UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                        return
                    }
                    return
                }
                
                if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200 {           // check for http errors
                    print("statusCode should be 200, but is \(httpStatus.statusCode)")
                    print("response = \(response)")
                    let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
                    print("responseString = \(responseString)")
                    NSOperationQueue.mainQueue().addOperationWithBlock {
                        let alert = UIAlertController(title: "Error", message: "Unknown error.", preferredStyle: UIAlertControllerStyle.Alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: nil))
                        self.presentViewController(alert, animated: true, completion: nil)
                        UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                        return
                    }
                }
                else
                {
                    
                    let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
                    print("responseString = \(responseString)")
                    self.getKeys(data!)
                    UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                    if(UserObject.privateKey!.isEmpty)
                    {
                        return
                    }
                    NSOperationQueue.mainQueue().addOperationWithBlock {
                        let consentAcceptViewController =  self.storyboard!.instantiateViewControllerWithIdentifier("ConsentAcceptViewController") as! ConsentAcceptViewController
                        self.presentViewController(consentAcceptViewController, animated: true, completion: nil)
                    }
                }
            }
            task.resume()
            
        } catch {
            print(error)
        }
    }
    
    func getKeys(data:NSData)
    {
        var err: NSError?
        do{
            
            var json = try NSJSONSerialization.JSONObjectWithData(data, options: .MutableLeaves) as? NSDictionary
            
            // Did the JSONObjectWithData constructor return an error? If so, log the error to the console
            if(err != nil) {
                print(err!.localizedDescription)
                let jsonStr = NSString(data: data, encoding: NSUTF8StringEncoding)
                print("Error could not parse JSON: '\(jsonStr)'")
                let alert = UIAlertController(title: "JSON Error", message: "Response is not in JSON format. Try again.", preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: nil))
                self.presentViewController(alert, animated: true, completion: nil)
                return
            }
            else {
                if let parseJSON = json {
                    if let key = parseJSON["public_key"]{
                        var pkey = key as! String
                        UserObject.publicKey = pkey
                    }
                    else
                    {
                        let alert = UIAlertController(title: "Response Error", message: "public_key is not returned. Try again.", preferredStyle: UIAlertControllerStyle.Alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: nil))
                        self.presentViewController(alert, animated: true, completion: nil)
                        return
                    }
                    if let key = parseJSON["private_key"]{
                        var pkey = key as! String
                        UserObject.privateKey = pkey
                    }
                    else
                    {
                        let alert = UIAlertController(title: "Response Error", message: "private_key is not returned. Try again.", preferredStyle: UIAlertControllerStyle.Alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: nil))
                        self.presentViewController(alert, animated: true, completion: nil)
                        return
                    }
                }
                else
                {
                    let alert = UIAlertController(title: "Response Error", message: "Response is empty. Try again.", preferredStyle: UIAlertControllerStyle.Alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: nil))
                    self.presentViewController(alert, animated: true, completion: nil)
                    return
                }
            }
        }catch{
            print(error)
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
