//
//  HomeViewController.swift
//  InfinitePay
//
//  Created by Balakrishnan on 6/10/16.
//  Copyright © 2016 FIS Global, Inc. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var balanceLabel: UILabel!

    @IBOutlet weak var offerBoxCentre: NSLayoutConstraint!
    @IBOutlet weak var offerBox: UIImageView!
    @IBOutlet weak var offerLabel1: UILabel!
    @IBOutlet weak var offerLabel2: UILabel!
    @IBOutlet weak var winButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.doLogin()
        

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(animated: Bool) {

        print("Inside viewWillAppear")
        super.viewWillAppear(animated) // No need for semicolon
        print(self.offerBoxCentre.constant)
        self.offerBoxCentre.constant += self.view.bounds.width
        print(self.offerBoxCentre.constant)
        if(!(UserObject.balance==nil))
        {
            self.balanceLabel.text = "Balance: $" + String(UserObject.balance!)
        }

    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        print("Inside viewDidAppear")
        UIView.animateWithDuration(2.0, delay: 1.0, options: UIViewAnimationOptions.CurveEaseInOut, animations: {
            print(self.offerBoxCentre.constant)
            self.offerBoxCentre.constant -= self.view.bounds.width
            print(self.offerBoxCentre.constant)
            self.view.layoutIfNeeded()
            }, completion: nil)
        
    }

    @IBAction func incrementBalance(sender: AnyObject) {
        
        self.updateBalance()
//        self.tabBarController!.selectedIndex = 1;
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func doLogin() {
        
        print("Inside doLogin")
        let userName:String = UserObject.username!
        var password:String
        if let val = DataRepository.credentials[userName] {
            
            password = val
        }
        else{
            password = "trunomi123"
        }
        
        print(userName)
        print(password)
        
        //                let request = NSMutableURLRequest(URL: NSURL(string: "https://fis-gcc-bankmock.trunomi.com/v1/bankmock/common/auth")!)
        
        let request = NSMutableURLRequest(URL: NSURL(string: "https://fis-devjam-test.apigee.net/tn/bm/v1/bankmock/common/auth")!)
        
        //        let request = NSMutableURLRequest(URL: NSURL(string: "https://fis-gcc-bankmock.trunomi.com/v1/bankmock/common/auth/")!)
        request.HTTPMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        //        var params = ["username":"mark_watney", "password":"trunomi123"] as Dictionary<String, String>
        //                let params = ["authToken":"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VybmFtZSI6Im1hcmtfd2F0bmV5IiwicGFzc3dvcmQiOiJ0cnVub21pMTIzIiwiaWF0IjoxNDY1MzgzOTkzLCJleHAiOjE0NjU0NzAzOTN9.nXXfbfk34Zwuu0opoTpKtGiFEwMOE7Br3ysZ27QXzGs"] as Dictionary<String, NSObject>
        var params = ["username":userName, "password":password] as Dictionary<String, String>
        
        //        let postString = "{\"username\": \"mark_watney\",\"password\": \"trunomi123\"}"
        //        let postString = "username=mark_watney&password=trunomi123".dataUsingEncoding(NSUTF8StringEncoding)
        //        let postString = "authToken=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VybmFtZSI6Im1hcmtfd2F0bmV5IiwicGFzc3dvcmQiOiJ0cnVub21pMTIzIiwiaWF0IjoxNDY1Mzc1MDczLCJleHAiOjE0NjU0NjE0NzN9.DX4XJzw-bEvlTh4aJwOvKbi_e5nfMQUGaExlUj0NikE".dataUsingEncoding(NSUTF8StringEncoding)
        
        //        let json = [ "username":"mark_watney" , "password": "trunomi123" ]
        
        
        
        do {
            let postString = try NSJSONSerialization.dataWithJSONObject(params, options: .PrettyPrinted)
            
            
            //        let postString = NSJSONSerialization.dataWithJSONObject(json, options: .PrettyPrinted)
            
            //        request.HTTPBody = postString.dataUsingEncoding(NSUTF8StringEncoding)
            //                request.setValue(String(request.HTTPBody?.length), forHTTPHeaderField: "Content-Length")
            request.HTTPBody = postString
            
            request.setValue(String(request.HTTPBody?.length), forHTTPHeaderField: "Content-Length")
            print(request.HTTPBody?.length)
            let task = NSURLSession.sharedSession().dataTaskWithRequest(request) { data, response, error in
                guard error == nil && data != nil else {                                                          // check for fundamental networking error
                    print("error=\(error)")
                    return
                }
                
                if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200 {           // check for http errors
                    print("statusCode should be 200, but is \(httpStatus.statusCode)")
                    print("response = \(response)")
                    let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
                    print("responseString = \(responseString)")
                }
                
                let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
                print("responseString = \(responseString)")
                var authToken = self.getAuthToken(data!)
                UserObject.authToken = authToken
                
                NSOperationQueue.mainQueue().addOperationWithBlock {
                    
                   self.updateProfile()
                }
                
            }
            task.resume()
            
        } catch {
            print(error)
        }
        
    }
    
    func getAuthToken(data:NSData) -> String
    {
        var authToken:String = "";
        var err: NSError?
        do{
            
            var json = try NSJSONSerialization.JSONObjectWithData(data, options: .MutableLeaves) as? NSDictionary
            
            // Did the JSONObjectWithData constructor return an error? If so, log the error to the console
            if(err != nil) {
                print(err!.localizedDescription)
                let jsonStr = NSString(data: data, encoding: NSUTF8StringEncoding)
                print("Error could not parse JSON: '\(jsonStr)'")
            }
            else {
                // The JSONObjectWithData constructor didn't return an error. But, we should still
                // check and make sure that json has a value using optional binding.
                if let parseJSON = json {
                    // Okay, the parsedJSON is here, let's get the value for 'success' out of it
                    authToken = parseJSON["authToken"] as! String
                    //                        print("Succes: \(success)")
                }
            }
        }catch {
            print(error)
        }
        
        return authToken
        
    }
    
    
    
    func updateProfile()
    {
        var authToken = UserObject.authToken!
        //let request = NSMutableURLRequest(URL: NSURL(string: "https://fis-gcc-bankmock.trunomi.com/v1/bankmock/cfa/app/truprofile")!)
        let request = NSMutableURLRequest(URL: NSURL(string: "https://fis-devjam-test.apigee.net/tn/bm/v1/bankmock/cfa/app/truprofile")!)
        request.HTTPMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        var params = ["authToken":authToken] as Dictionary<String, String>
        do
        {
            let postString = try NSJSONSerialization.dataWithJSONObject(params, options: .PrettyPrinted)
            request.HTTPBody = postString
            request.setValue(String(request.HTTPBody?.length), forHTTPHeaderField: "Content-Length")
            print(request.HTTPBody?.length)
            let task = NSURLSession.sharedSession().dataTaskWithRequest(request) { data, response, error in
                guard error == nil && data != nil else {                                                          // check for fundamental networking error
                    print("error=\(error)")
                    return
                }
                
                if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200 {           // check for http errors
                    print("statusCode should be 200, but is \(httpStatus.statusCode)")
                    print("response = \(response)")
                    let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
                    print("responseString = \(responseString)")
                }
                
                let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
                print("responseString = \(responseString)")
                var name = self.getName(data!)
                var address = self.getAddress(data!)
                var email = self.getEmail(data!)
//                var consumerID = self.getConsumerID(data!)

                print(name)
                print(address)
                print(email)
                
//                self.nameLabel.performSelector("text", withObject: name)
//                self.nameLabel.text
                NSOperationQueue.mainQueue().addOperationWithBlock {
                    
                self.getBalance()
                self.nameLabel.text = name
                self.addressLabel.text = address
                self.emailLabel.text = email
//                UserObject.consumerID = consumerID
                }
//                UserObject.authToken = authToken
            }
            task.resume()
            
        } catch {
            print(error)
        }
        
        
        

    }
    
    func getAddress(data:NSData) -> String
    {
        var address:String = "";
        var err: NSError?
        do{
            
            var json = try NSJSONSerialization.JSONObjectWithData(data, options: .MutableLeaves) as? NSDictionary
            
            // Did the JSONObjectWithData constructor return an error? If so, log the error to the console
            if(err != nil) {
                print(err!.localizedDescription)
                let jsonStr = NSString(data: data, encoding: NSUTF8StringEncoding)
                print("Error could not parse JSON: '\(jsonStr)'")
            }
            else {
                // The JSONObjectWithData constructor didn't return an error. But, we should still
                // check and make sure that json has a value using optional binding.
                if let parseJSON = json {
                    // Okay, the parsedJSON is here, let's get the value for 'success' out of it
                    address = parseJSON["ConsumerDetails"]!["address"] as! String
                    //                        print("Succes: \(success)")
                }
            }
        }catch {
            print(error)
        }
        
        return address
        
    }
    
    func getConsumerID(data:NSData) -> String
    {
        var consumerID:String = "";
        var err: NSError?
        do{
            
            var json = try NSJSONSerialization.JSONObjectWithData(data, options: .MutableLeaves) as? NSDictionary
            
            // Did the JSONObjectWithData constructor return an error? If so, log the error to the console
            if(err != nil) {
                print(err!.localizedDescription)
                let jsonStr = NSString(data: data, encoding: NSUTF8StringEncoding)
                print("Error could not parse JSON: '\(jsonStr)'")
            }
            else {
                // The JSONObjectWithData constructor didn't return an error. But, we should still
                // check and make sure that json has a value using optional binding.
                if let parseJSON = json {
                    // Okay, the parsedJSON is here, let's get the value for 'success' out of it
                    consumerID = parseJSON["ConsumerDetails"]!["consumer_id"] as! String
                    //                        print("Succes: \(success)")
                }
            }
        }catch {
            print(error)
        }
        
        return consumerID
        
    }
    
    func getEmail(data:NSData) -> String
    {
        var email:String = "";
        var err: NSError?
        do{
            
            var json = try NSJSONSerialization.JSONObjectWithData(data, options: .MutableLeaves) as? NSDictionary
            
            // Did the JSONObjectWithData constructor return an error? If so, log the error to the console
            if(err != nil) {
                print(err!.localizedDescription)
                let jsonStr = NSString(data: data, encoding: NSUTF8StringEncoding)
                print("Error could not parse JSON: '\(jsonStr)'")
            }
            else {
                // The JSONObjectWithData constructor didn't return an error. But, we should still
                // check and make sure that json has a value using optional binding.
                if let parseJSON = json {
                    // Okay, the parsedJSON is here, let's get the value for 'success' out of it
                    email = parseJSON["ConsumerDetails"]!["email"] as! String
                    //                        print("Succes: \(success)")
                }
            }
        }catch {
            print(error)
        }
        
        return email
        
    }


    
    func getName(data:NSData) -> String
    {
        var name:String = "";
        var err: NSError?
        do{
            
            var json = try NSJSONSerialization.JSONObjectWithData(data, options: .MutableLeaves) as? NSDictionary
            
            // Did the JSONObjectWithData constructor return an error? If so, log the error to the console
            if(err != nil) {
                print(err!.localizedDescription)
                let jsonStr = NSString(data: data, encoding: NSUTF8StringEncoding)
                print("Error could not parse JSON: '\(jsonStr)'")
            }
            else {
                // The JSONObjectWithData constructor didn't return an error. But, we should still
                // check and make sure that json has a value using optional binding.
                if let parseJSON = json {
                    // Okay, the parsedJSON is here, let's get the value for 'success' out of it
                    name = parseJSON["ConsumerDetails"]!["first_name"] as! String
                    name += " "
                    name += parseJSON["ConsumerDetails"]!["last_name"] as! String
                    //                        print("Succes: \(success)")
                    
//                    self.nameLabel.performSelector("text", withObject: name)
//                    self.nameLabel.text = name
//                    print(self.nameLabel.text)

                }
            }
        }catch {
            print(error)
        }
        
        return name
        
    }

    func getBalance(data:NSData) -> NSInteger
    {
        var balance:NSInteger = 12
        var err: NSError?
        do{
            
            var json = try NSJSONSerialization.JSONObjectWithData(data, options: .MutableLeaves) as? NSDictionary
            
            // Did the JSONObjectWithData constructor return an error? If so, log the error to the console
            if(err != nil) {
                print(err!.localizedDescription)
                let jsonStr = NSString(data: data, encoding: NSUTF8StringEncoding)
                print("Error could not parse JSON: '\(jsonStr)'")
            }
            else {
                // The JSONObjectWithData constructor didn't return an error. But, we should still
                // check and make sure that json has a value using optional binding.
                if let parseJSON = json {
                    // Okay, the parsedJSON is here, let's get the value for 'success' out of it
                    balance = parseJSON["balance"] as! NSInteger
                    //                        print("Succes: \(success)")
                }
            }
        }catch {
            print(error)
        }
        
        return balance
        
    }
    
    func getBalance()
    {
        var authToken = UserObject.authToken!
        //let request = NSMutableURLRequest(URL: NSURL(string: "https://fis-gcc-bankmock.trunomi.com/v1/bankmock/cfa/app/account_balance")!)
        let request = NSMutableURLRequest(URL: NSURL(string: "https://fis-devjam-test.apigee.net/tn/bm/v1/bankmock/cfa/app/account_balance")!)
        
        request.HTTPMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        var params = ["authToken":authToken] as Dictionary<String, String>
        do
        {
            let postString = try NSJSONSerialization.dataWithJSONObject(params, options: .PrettyPrinted)
            request.HTTPBody = postString
            request.setValue(String(request.HTTPBody?.length), forHTTPHeaderField: "Content-Length")
            print(request.HTTPBody?.length)
            let task = NSURLSession.sharedSession().dataTaskWithRequest(request) { data, response, error in
                guard error == nil && data != nil else {                                                          // check for fundamental networking error
                    print("error=\(error)")
                    return
                }
                
                if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200 {           // check for http errors
                    print("statusCode should be 200, but is \(httpStatus.statusCode)")
                    print("response = \(response)")
                    let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
                    print("responseString = \(responseString)")
                }
                
                let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
                print("responseString = \(responseString)")
                var balance = self.getBalance(data!)
                
                //                self.nameLabel.performSelector("text", withObject: name)
                //                self.nameLabel.text
                NSOperationQueue.mainQueue().addOperationWithBlock {
                    self.balanceLabel.text =  "Balance: $" + String(balance)
                    UserObject.balance = balance
                    //                    UserObject.consumerID = consumerID
                }
                //                UserObject.authToken = authToken
            }
            task.resume()
            
        } catch {
            print(error)
        }
    }
   
    
    
    func updateBalance()
    {
        var authToken = UserObject.authToken!
        //let request = NSMutableURLRequest(URL: NSURL(string: "https://fis-gcc-bankmock.trunomi.com/v1/bankmock/cfa/app/account_balance")!)
        let request = NSMutableURLRequest(URL: NSURL(string: "https://fis-devjam-test.apigee.net/tn/bm/v1/bankmock/cfa/app/account_balance")!)
        
        request.HTTPMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        var params = ["authToken":authToken, "set": (10 + UserObject.balance!)] as Dictionary<String, NSObject>
        do
        {
            let postString = try NSJSONSerialization.dataWithJSONObject(params, options: .PrettyPrinted)
            request.HTTPBody = postString
            request.setValue(String(request.HTTPBody?.length), forHTTPHeaderField: "Content-Length")
            print(request.HTTPBody?.length)
            let task = NSURLSession.sharedSession().dataTaskWithRequest(request) { data, response, error in
                guard error == nil && data != nil else {                                                          // check for fundamental networking error
                    print("error=\(error)")
                    return
                }
                
                if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200 {           // check for http errors
                    print("statusCode should be 200, but is \(httpStatus.statusCode)")
                    print("response = \(response)")
                    let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
                    print("responseString = \(responseString)")
                }
                
                let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
                print("responseString = \(responseString)")
                var balance = self.getBalance(data!)
                
                //                self.nameLabel.performSelector("text", withObject: name)
                //                self.nameLabel.text
                NSOperationQueue.mainQueue().addOperationWithBlock {
                    self.balanceLabel.text =  "Balance: $" + String(balance)
                    UserObject.balance = balance
                    
                    let alert = UIAlertController(title: "$10 added", message: "$10 successfully added to your Infinite Pay Wallet.", preferredStyle: UIAlertControllerStyle.Alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { action in
                        switch action.style{
                        case .Default:
                            print("default")
                            UIView.animateWithDuration(2.0, delay: 1.0, options: UIViewAnimationOptions.CurveEaseInOut, animations: {
                                self.winButton.alpha = 0.5
                                print(self.offerBoxCentre.constant)
                                self.offerBoxCentre.constant += self.view.bounds.width
                                print(self.offerBoxCentre.constant)
                                self.view.layoutIfNeeded()
                                }, completion: {
                                (value: Bool) in
                                    self.offerBox.hidden = true
                                    self.offerLabel1.hidden = true
                                    self.offerLabel2.hidden = true
                                    self.winButton.hidden = true
//                                    self.makePaymentButton.enabled = false
                                })
                            
                        case .Cancel:
                            print("cancel")
                            
                        case .Destructive:
                            print("destructive")
                        }
                    }))
                    self.presentViewController(alert, animated: true, completion: nil)
                }
                //                UserObject.authToken = authToken
            }
            task.resume()
            
        } catch {
            print(error)
        }
    }
    

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
