//
//  DataRepository.swift
//  InfinitePay
//
//  Created by Balakrishnan on 6/24/16.
//  Copyright © 2016 FIS Global, Inc. All rights reserved.
//

import UIKit

class DataRepository: NSObject {
    
    static var credentials:[String:String] = ["sujay_dutta": "foxtrot",
                                              "vivek_kumar" : "abcdefg",
                                              "scott_meyer" :           "theoretic",
                                              "balakrishnan_ramasamy" : "metal",
                                              "andy_roe" :              "games",
                                              "mark_watney": "trunomi123"]
    
    static let banckMockBaseURL:String = "https://fis-devjam-test.apigee.net/tn/bm"
    static let trunomiBaseURL:String = "https://fis-devjam-test.apigee.net/tn/tpb"
    
    static let loginURL = banckMockBaseURL + "/v1/bankmock/common/auth"
    static let profileURL = banckMockBaseURL + "/v1/bankmock/cfa/app/truprofile"
    static let consumerNameURL = banckMockBaseURL + "/v1/bankmock/efa/getconsumername"
    static let balanceURL = banckMockBaseURL + "/v1/bankmock/cfa/app/account_balance"
    static let triggerPiiURL = banckMockBaseURL + "/v1/bankmock/cfa/app/trigger_pii"
    static let triggerConsentURL = banckMockBaseURL + "/v1/bankmock/cfa/app/trigger_consent"
    static let createUserURL = banckMockBaseURL + "/v1/bankmock/cfa/app/create_user"
    static let consumerKeysURL = banckMockBaseURL + "/v1/bankmock/cfa/app/getconsumerkeys"


}
