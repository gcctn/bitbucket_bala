//
//  ConsentAcceptViewController.swift
//  InfinitePay
//
//  Created by Balakrishnan on 6/8/16.
//  Copyright © 2016 FIS Global, Inc. All rights reserved.
//
import UIKit
import WebKit
//import UserObject

class ConsentAcceptViewController: UIViewController, WKScriptMessageHandler {
    
    @IBOutlet weak var acceptSwitch: UISwitch!
    @IBOutlet weak var submitButton: UIButton!
    
    //    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    //    var namePiiId:String = "";
    //    var emailPiiId:String = "";
    //    var addressPiiId:String = "";
    
    var consentId:String = ""
    
    var bTriggerConsent:Bool = false
    
    @IBOutlet weak var webView: UIWebView!
    var webKitView: WKWebView?;
    var scriptController: WKUserContentController?
    
    func getScriptController() -> WKUserContentController {
        if (self.scriptController == nil){
            self.scriptController = WKUserContentController();
        }
        return self.scriptController!;
    }
    
    func addScriptToWK(script: String) {
        let scriptController = getScriptController();
        let startAuth = WKUserScript(
            source: script,
            injectionTime: WKUserScriptInjectionTime.AtDocumentStart,
            forMainFrameOnly: true
        );
        scriptController.addUserScript(startAuth);
    }

    func clearWKCache() -> Void {
        if #available(iOS 9.0, *) {
            let websiteDataTypes = NSSet(array: [WKWebsiteDataTypeDiskCache, WKWebsiteDataTypeMemoryCache])
            let date = NSDate(timeIntervalSince1970: 0)
            WKWebsiteDataStore.defaultDataStore().removeDataOfTypes(websiteDataTypes as! Set<String>, modifiedSince: date, completionHandler:{ })
        }
    }
    
    override func loadView(){
        super.loadView();
        clearWKCache();
        //        prepareResources();
        let config = WKWebViewConfiguration();
        config.userContentController = getScriptController();
        config.userContentController.addScriptMessageHandler(self, name: "console");
        config.userContentController.addScriptMessageHandler(self, name: "authConsole");
        config.userContentController.addScriptMessageHandler(self, name: "piiConsole");
        config.userContentController.addScriptMessageHandler(self, name: "endPiiConsole");
        config.userContentController.addScriptMessageHandler(self, name: "transactConsole");
        
        self.webKitView = WKWebView(frame: self.webView.bounds, configuration: config);
        self.webView.addSubview(webKitView!);
    }
    
    func userContentController(userContentController: WKUserContentController, didReceiveScriptMessage message: WKScriptMessage) {
        
        print(message.name)
        
        if(message.name=="console")
        {
            print(message.body)
        }
        else if(message.name=="authConsole")
        {
            print(message.body)
            if((message.body as! String) == "Ready"){
                print("Got Ready");
                self.webKitView?.evaluateJavaScript("getTransactions();"){ (result, error) in
                    if error != nil {
                        print(result)
                    }
                }
                self.webKitView?.evaluateJavaScript("getPiis();"){ (result, error) in
                    if error != nil {
                        print(result)
                    }
                }
                
            }
        }
        else if(message.name=="piiConsole")
        {
            
            //            namePiiId = "name"
            //            addressPiiId = "address"
            //            emailPiiId = "email"
            print(message.body)
            
            
            var err: NSError?
            let messagebody = message.body.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)!
            do{
                var json = try NSJSONSerialization.JSONObjectWithData(messagebody, options: .MutableLeaves) as? NSDictionary
                
                // Did the JSONObjectWithData constructor return an error? If so, log the error to the console
                if(err != nil)
                {
                    print(err!.localizedDescription)
                    let jsonStr = NSString(data: messagebody, encoding: NSUTF8StringEncoding)
                    print("Error could not parse JSON: '\(jsonStr)'")
                }
                else
                {
                    // The JSONObjectWithData constructor didn't return an error. But, we should still
                    // check and make sure that json has a value using optional binding.
                    if let parseJSON = json {
                        // Okay, the parsedJSON is here, let's get the value for 'success' out of it
                        var piiId = parseJSON["document"]!["id"] as! String
                        var type = parseJSON["document"]!["type"] as! String
                        if(type=="name" || type=="address" || type=="email" || type=="account details")
                        {
                            UserObject.piiId[type] = piiId
                        }
                        //                        if((!UserObject.piiId["name"]!.isEmpty) && (!UserObject.piiId["address"]!.isEmpty) && (!UserObject.piiId["email"]!.isEmpty))
                        //                        {
                        //                            scriptExecuted = true;
                        //                            acceptSwitch.enabled = true;
                        //                        }
                        print(piiId)
                        print(type)
                        //                        print("Succes: \(success)")
                    }
                }
            }catch {
                print(error)
            }
            
        }
        else if(message.name=="endPiiConsole")
        {
            print(message.body)
            if(!bTriggerConsent)
            {
                bTriggerConsent = true;
                doLogin()
            }
        }
        else if(message.name=="transactConsole")
        {
            print(message.body)
        }
        else if(message.name=="grantConsentConsole")
        {
            print(message.body)
            UIApplication.sharedApplication().networkActivityIndicatorVisible = false
        }
        
    }
    
    
    override func viewDidLoad() {
        
        print("Inside viewDidLoad")
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        //        self.spinner.startAnimating()
        //        self.spinner.hidden = false
        super.viewDidLoad()
        
        
        //        super.viewDidLoad()
        //        let url = NSURL.fileURLWithPath("\(NSHomeDirectory())/Library/Cache/Assets/index.html");
        let url = NSURL.fileURLWithPath(NSBundle.mainBundle().pathForResource("index", ofType: "html")!)
        let req = NSURLRequest(URL: url);
        self.webKitView!.loadRequest(req);
        //        let pathToJS = "\(NSHomeDirectory())/Library/Cache/Assets/bundle.js";
        let pathToJS = NSBundle.mainBundle().pathForResource("bundle", ofType: "js")!
        let js = try! String(contentsOfFile: pathToJS, encoding: NSUTF8StringEncoding);
        addScriptToWK(js);
        let userName:String = UserObject.username!
        var password:String
        if let val = DataRepository.credentials[userName] {
            
            password = val
        }
        else{
            password = "trunomi123"
        }
        
        print(userName)
        print(password)
        print("Adding Script to WK");
        print(UserObject.publicKey!);
        let publicKey = UserObject.publicKey!.dataUsingEncoding(NSUTF8StringEncoding)?.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0));
        
        let privateKey = UserObject.privateKey!.dataUsingEncoding(NSUTF8StringEncoding)?.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0));
        print(publicKey);
        addScriptToWK("window.username = '" + userName + "'");
        addScriptToWK("window.authToken = '" + UserObject.authToken! + "'");
        addScriptToWK("window.enterpriseID = '" + UserObject.enterpriseID! + "'");
        addScriptToWK("window.displayName = '" + UserObject.displayName! + "'");
        addScriptToWK("window.publicKey = '" + publicKey! + "'");
        addScriptToWK("window.privateKey = '" + privateKey! + "'");
        
//        addScriptToWK("username = '" + userName + "'");
//        addScriptToWK("authToken = '" + UserObject.authToken! + "'");
//        addScriptToWK("enterpriseID = '" + UserObject.enterpriseID! + "'");
//        addScriptToWK("displayName = '" + UserObject.displayName! + "'");
//        addScriptToWK("publicKey = '" + UserObject.publicKey! + "'");
//        addScriptToWK("privateKey = '" + UserObject.privateKey! + "'");
        
        addScriptToWK("startAuth('" + userName + "', '" + password + "')");
        addScriptToWK("getPii()");
        
        acceptSwitch.enabled = false;
        print("End viewDidLoad")
        //        addScriptToWK("startTransact()");
        
        
        
        //        var piiId_Name = getPiiFromTrunomi("name")
        //        var piiId_Address = getPiiFromTrunomi("address")
        //        var piiId_Email = getPiiFromTrunomi("email")
        //
        ////        let group = dispatch_group_create()
        //
        //        if(piiId_Name.isEmpty)
        //                {
        //                    self.triggerPii("name")
        //                    }
        
        //        dispatch_group_async(group, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0)) { () -> Void in
        //            if(piiId_Name.isEmpty)
        //        {
        //            self.triggerPii("name")
        //            }}
        //        dispatch_group_async(group, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0)) { () -> Void in
        //        if(piiId_Address.isEmpty)
        //        {
        //            self.triggerPii("address")
        //        }
        //        }
        //        dispatch_group_async(group, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0)) { () -> Void in
        //        if(piiId_Email.isEmpty)
        //        {
        //            self.triggerPii("email")
        //        }
        //        }
        //
        //        dispatch_group_async(group, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0)) { () -> Void in
        //        self.triggerConsent([UserObject.piiId["name"]!, UserObject.piiId["address"]!, UserObject.piiId["email"]!])
        //        }
        //
        
        
        
        if(acceptSwitch.on)
        {
            print("Switch is ON")
            submitButton.enabled = true
            submitButton.alpha = 1.0
            
        }
        else
        {
            print("Switch is OFF")
            submitButton.enabled = false
            submitButton.alpha = 0.5
        }
        
        
        // Do any additional setup after loading the view.
        
    }
    
    //Calls this function when the tap is recognized.
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    @IBAction func doAccept(sender: AnyObject) {
        
        if(acceptSwitch.on)
        {
            print("Switch is ON")
            //            while(scriptExecuted==false)
            //            {
            //                sleep(1)
            //            }
            
            
            
            //            self.triggerPii("name")
            
            //            while(self.namePiiId.isEmpty || self.emailPiiId.isEmpty || self.addressPiiId.isEmpty)
            //            {
            //                sleep(1)
            //            }
            //            if(self.namePiiId.name)
            //            print(UserObject.customerName)
            if(grantConsentToTrunomi())
            {
                submitButton.enabled = true
                submitButton.alpha = 1.0
            }
        }
        else
        {
            print("Switch is OFF")
            submitButton.enabled = false
            submitButton.alpha = 0.5
        }
        
    }
    
    func triggerPii(field: String) {
        
        print("Inside triggerPii: " + field)
        
        let request = NSMutableURLRequest(URL: NSURL(string: DataRepository.triggerPiiURL)!)
        
        
        request.HTTPMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        var params = ["authToken":UserObject.authToken!, "name":field] as Dictionary<String, String>
        //        print(DataRepository.triggerPiiURL)
        //        print("authToken: " + UserObject.authToken! + "name: " + field)
        
        
        do {
            let postString = try NSJSONSerialization.dataWithJSONObject(params, options: .PrettyPrinted)
            
            
            request.HTTPBody = postString
            
            request.setValue(String(request.HTTPBody?.length), forHTTPHeaderField: "Content-Length")
            print(request.HTTPBody?.length)
            let task = NSURLSession.sharedSession().dataTaskWithRequest(request) { data, response, error in
                guard error == nil && data != nil else {                                                          // check for fundamental networking error
                    print("error=\(error)")
                    return
                }
                
                if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200 {           // check for http errors
                    print("statusCode should be 200, but is \(httpStatus.statusCode)")
                    print("response = \(response)")
                    let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
                    print("responseString = \(responseString)")
                }
                
                let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
                print("responseString = \(responseString)")
                var piiId = self.getPiiId(data!)
                UserObject.piiId[field] = piiId
                
                NSOperationQueue.mainQueue().addOperationWithBlock {
                    
                    if(field=="name")
                    {
                        if(UserObject.piiId["address"]!.isEmpty)
                        {
                            self.triggerPii("address")
                        }
                        else if(UserObject.piiId["email"]!.isEmpty)
                        {
                            self.triggerPii("email")
                        }
                        else if(UserObject.piiId["account details"]!.isEmpty)
                        {
                            self.triggerPii("account details")
                        }
                        else
                        {
                            self.triggerConsent([UserObject.piiId["name"]!, UserObject.piiId["address"]!, UserObject.piiId["email"]!, UserObject.piiId["account details"]!])
                        }
                    }
                    else if(field=="address")
                    {
                        if(UserObject.piiId["email"]!.isEmpty)
                        {
                            self.triggerPii("email")
                        }
                        else if(UserObject.piiId["account details"]!.isEmpty)
                        {
                            self.triggerPii("account details")
                        }
                        else
                        {
                            self.triggerConsent([UserObject.piiId["name"]!, UserObject.piiId["address"]!, UserObject.piiId["email"]!, UserObject.piiId["account details"]!])
                        }
                    }
                    else if(field=="email")
                    {
                        if(UserObject.piiId["account details"]!.isEmpty)
                        {
                            self.triggerPii("account details")
                        }
                        else
                        {
                            self.triggerConsent([UserObject.piiId["name"]!, UserObject.piiId["address"]!, UserObject.piiId["email"]!, UserObject.piiId["account details"]!])
                        }
                    }
                    else if(field=="account details")
                    {
                        self.triggerConsent([UserObject.piiId["name"]!, UserObject.piiId["address"]!, UserObject.piiId["email"]!, UserObject.piiId["account details"]!])
                    }
                }
            }
            task.resume()
            
        } catch {
            print(error)
        }
        
    }
    
    func getPiiId(data:NSData) -> String
    {
        var piiId:String = "";
        var err: NSError?
        do{
            var json = try NSJSONSerialization.JSONObjectWithData(data, options: .MutableLeaves) as? NSDictionary
            
            // Did the JSONObjectWithData constructor return an error? If so, log the error to the console
            if(err != nil) {
                print(err!.localizedDescription)
                let jsonStr = NSString(data: data, encoding: NSUTF8StringEncoding)
                print("Error could not parse JSON: '\(jsonStr)'")
            }
            else {
                // The JSONObjectWithData constructor didn't return an error. But, we should still
                // check and make sure that json has a value using optional binding.
                if let parseJSON = json {
                    // Okay, the parsedJSON is here, let's get the value for 'success' out of it
                    piiId = parseJSON["id"] as! String
                    //                        print("Succes: \(success)")
                }
            }
        }catch {
            print(error)
        }
        
        return piiId
        
    }
    
    
    func triggerConsent(piiIds: [String]) {
        
        print("Inside triggerConsent")
        
        let request = NSMutableURLRequest(URL: NSURL(string: DataRepository.triggerConsentURL)!)
        
        request.HTTPMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        var params = ["authToken":UserObject.authToken!, "subject":"GCC App", "piiIDs":piiIds] as Dictionary<String, NSObject>
        
        do {
            let postString = try NSJSONSerialization.dataWithJSONObject(params, options: .PrettyPrinted)
            
            
            request.HTTPBody = postString
            
            request.setValue(String(request.HTTPBody?.length), forHTTPHeaderField: "Content-Length")
            print(request.HTTPBody?.length)
            let task = NSURLSession.sharedSession().dataTaskWithRequest(request) { data, response, error in
                guard error == nil && data != nil else {                                                          // check for fundamental networking error
                    print("error=\(error)")
                    return
                }
                
                if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200 {           // check for http errors
                    print("statusCode should be 200, but is \(httpStatus.statusCode)")
                    print("response = \(response)")
                    let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
                    print("responseString = \(responseString)")
                }
                
                let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
                var consentId = self.getConsentId(data!)
                print("responseString = \(responseString)")
                self.consentId = consentId
                dispatch_async(dispatch_get_main_queue(), {
                    self.acceptSwitch.enabled = true
                    UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                    //                    self.spinner.stopAnimating()
                    //                    self.spinner.hidden = true
                })
                print("Switch Enabled")
                //                UserObject.piiId[field] = piiId
                
            }
            task.resume()
            
        } catch {
            print(error)
        }
        
        
        
    }
    
    func getConsentId(data:NSData) -> String
    {
        var consentId:String = "";
        var err: NSError?
        do{
            var json = try NSJSONSerialization.JSONObjectWithData(data, options: .MutableLeaves) as? NSDictionary
            
            // Did the JSONObjectWithData constructor return an error? If so, log the error to the console
            if(err != nil) {
                print(err!.localizedDescription)
                let jsonStr = NSString(data: data, encoding: NSUTF8StringEncoding)
                print("Error could not parse JSON: '\(jsonStr)'")
            }
            else {
                // The JSONObjectWithData constructor didn't return an error. But, we should still
                // check and make sure that json has a value using optional binding.
                if let parseJSON = json {
                    // Okay, the parsedJSON is here, let's get the value for 'success' out of it
                    consentId = parseJSON["id"] as! String
                    //                        print("Succes: \(success)")
                }
            }
        }catch {
            print(error)
        }
        
        return consentId
        
    }
    
    func grantConsentToTrunomi() -> (BooleanType){
        
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        
        // Logic from second App.
        
        //        let config = WKWebViewConfiguration();
        //        config.userContentController = getScriptController();
        //        getScriptController().addScriptMessageHandler(self, name: "console");
        getScriptController().removeScriptMessageHandlerForName("piiConsole");
        getScriptController().removeScriptMessageHandlerForName("endPiiConsole");
        getScriptController().removeScriptMessageHandlerForName("transactConsole");
        getScriptController().removeAllUserScripts()
        getScriptController().addScriptMessageHandler(self, name: "grantConsentConsole");
        
        
        //        self.webKitView = WKWebView(frame: self.webView.bounds, configuration: config);
        //        self.webView.addSubview(webKitView!);
        
        
        let url = NSURL.fileURLWithPath(NSBundle.mainBundle().pathForResource("index", ofType: "html")!)
        let req = NSURLRequest(URL: url);
        
        //        let pathToJS = "\(NSHomeDirectory())/Library/Cache/Assets/bundle.js";
        let pathToJS = NSBundle.mainBundle().pathForResource("grantConsent", ofType: "js")!
        let js = try! String(contentsOfFile: pathToJS, encoding: NSUTF8StringEncoding);
        addScriptToWK(js);
        //        addScriptToWK("startAuth('balakrishnan_ramasamy', 'metal')");
        let userName:String = UserObject.username!
        var password:String
        if let val = DataRepository.credentials[userName] {
            
            password = val
        }
        else{
            password = "trunomi123"
        }
        
        print(userName)
        print(password)
        
        
        addScriptToWK("startAuth('" + userName + "', '" + password + "')");
        //        print(self.consentId)
        //        addScriptToWK("startTransact('" + self.consentId + "')");
        self.webKitView!.loadRequest(req);
        //        print("End viewDidLoad")
        //
        //
        //        addScriptToWK("grantConsentiOs()")
        
        
        return true
    }
    
    
    func doLogin() {
        
        print("Inside doLogin")
        let userName:String = UserObject.username!
        var password:String
        if let val = DataRepository.credentials[userName] {
            
            password = val
        }
        else{
            password = "trunomi123"
        }
        
        print(userName)
        print(password)
        
        //                let request = NSMutableURLRequest(URL: NSURL(string: "https://fis-gcc-bankmock.trunomi.com/v1/bankmock/common/auth")!)
        
        let request = NSMutableURLRequest(URL: NSURL(string: "https://fis-devjam-test.apigee.net/tn/bm/v1/bankmock/common/auth")!)
        
        //        let request = NSMutableURLRequest(URL: NSURL(string: "https://fis-gcc-bankmock.trunomi.com/v1/bankmock/common/auth/")!)
        request.HTTPMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        //        var params = ["username":"mark_watney", "password":"trunomi123"] as Dictionary<String, String>
        //                let params = ["authToken":"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VybmFtZSI6Im1hcmtfd2F0bmV5IiwicGFzc3dvcmQiOiJ0cnVub21pMTIzIiwiaWF0IjoxNDY1MzgzOTkzLCJleHAiOjE0NjU0NzAzOTN9.nXXfbfk34Zwuu0opoTpKtGiFEwMOE7Br3ysZ27QXzGs"] as Dictionary<String, NSObject>
        var params = ["username":userName, "password":password] as Dictionary<String, String>
        
        //        let postString = "{\"username\": \"mark_watney\",\"password\": \"trunomi123\"}"
        //        let postString = "username=mark_watney&password=trunomi123".dataUsingEncoding(NSUTF8StringEncoding)
        //        let postString = "authToken=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VybmFtZSI6Im1hcmtfd2F0bmV5IiwicGFzc3dvcmQiOiJ0cnVub21pMTIzIiwiaWF0IjoxNDY1Mzc1MDczLCJleHAiOjE0NjU0NjE0NzN9.DX4XJzw-bEvlTh4aJwOvKbi_e5nfMQUGaExlUj0NikE".dataUsingEncoding(NSUTF8StringEncoding)
        
        //        let json = [ "username":"mark_watney" , "password": "trunomi123" ]
        
        
        
        do {
            let postString = try NSJSONSerialization.dataWithJSONObject(params, options: .PrettyPrinted)
            
            
            //        let postString = NSJSONSerialization.dataWithJSONObject(json, options: .PrettyPrinted)
            
            //        request.HTTPBody = postString.dataUsingEncoding(NSUTF8StringEncoding)
            //                request.setValue(String(request.HTTPBody?.length), forHTTPHeaderField: "Content-Length")
            request.HTTPBody = postString
            
            request.setValue(String(request.HTTPBody?.length), forHTTPHeaderField: "Content-Length")
            print(request.HTTPBody?.length)
            let task = NSURLSession.sharedSession().dataTaskWithRequest(request) { data, response, error in
                guard error == nil && data != nil else {                                                          // check for fundamental networking error
                    print("error=\(error)")
                    return
                }
                
                if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200 {           // check for http errors
                    print("statusCode should be 200, but is \(httpStatus.statusCode)")
                    print("response = \(response)")
                    let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
                    print("responseString = \(responseString)")
                }
                
                let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
                print("responseString = \(responseString)")
                var authToken = self.getAuthToken(data!)
                UserObject.authToken = authToken
                
                NSOperationQueue.mainQueue().addOperationWithBlock {
                    
                    if(UserObject.piiId["name"]!.isEmpty)
                    {
                        self.triggerPii("name")
                    }
                    else if(UserObject.piiId["address"]!.isEmpty)
                    {
                        self.triggerPii("address")
                    }
                    else if(UserObject.piiId["email"]!.isEmpty)
                    {
                        self.triggerPii("email")
                    }
                    else if(UserObject.piiId["account details"]!.isEmpty)
                    {
                        self.triggerPii("account details")
                    }
                    else
                    {
                        self.triggerConsent([UserObject.piiId["name"]!, UserObject.piiId["address"]!, UserObject.piiId["email"]!, UserObject.piiId["account details"]!])
                    }
                }
                
            }
            task.resume()
            
        } catch {
            print(error)
        }
        
    }
    
    func getAuthToken(data:NSData) -> String
    {
        var authToken:String = "";
        var err: NSError?
        do{
            
            var json = try NSJSONSerialization.JSONObjectWithData(data, options: .MutableLeaves) as? NSDictionary
            
            // Did the JSONObjectWithData constructor return an error? If so, log the error to the console
            if(err != nil) {
                print(err!.localizedDescription)
                let jsonStr = NSString(data: data, encoding: NSUTF8StringEncoding)
                print("Error could not parse JSON: '\(jsonStr)'")
            }
            else {
                // The JSONObjectWithData constructor didn't return an error. But, we should still
                // check and make sure that json has a value using optional binding.
                if let parseJSON = json {
                    // Okay, the parsedJSON is here, let's get the value for 'success' out of it
                    authToken = parseJSON["authToken"] as! String
                    //                        print("Succes: \(success)")
                }
            }
        }catch {
            print(error)
        }
        
        return authToken
        
    }
    
    
    
    
    
}
